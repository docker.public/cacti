# Cacit 1.x Docker image
##### Based on work done by Sean Cline (https://github.com/scline/docker-cacti)

##### Download cacti-1.x.tar.gz and cacti-spine-1.x.tar.gz and copy files into directory cacti/
+ https://www.cacti.net/downloads/cacti-1.1.38.tar.gz
+ https://www.cacti.net/downloads/spine/cacti-spine-1.1.38.tar.gz

## Build Image
```
docker build -t americatel/amp-cacti .
```

## Run container MySQL database
```
docker run -d --name=cacti-db \
	-v cacti-db:/var/lib/mysql \
	-e MYSQL_DATABASE=cacti \
	-e MYSQL_USER=cactiuser \
	-e MYSQL_PASSWORD=password \
	-e MYSQL_ROOT_PASSWORD=password \
	mysql:5.7
```

## Copy mysql.cnf to container
```	
docker cp configs/mysql.cnf cacti-db:/etc/mysql/mysql.cnf
docker restart cacti-db
```

## Run container Cacti
```	
docker run -d --name cacti-web \
	--link=cacti-db:cacti-db \
	-p 8000:80 \
	-p 4443:443 \
	-e DB_HOST=cacti-db \
	-e DB_NAME=cacti \
	-e DB_USER=cactiuser \
	-e DB_PASS=password \
	-e DB_ROOT_PASS=password \
	-e INITIALIZE_DB=1 \
	-e TZ=America/Lima \
	americatel/amp-cacti
```	

## Open URL in your Web Browser
```	
http://[HOST_IP]:8000/cacti
```